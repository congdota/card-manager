package sapo.demo.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "config")
@Data
public class Config implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "config_point")
    private Double configPoint;
}
