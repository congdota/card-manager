package sapo.demo.Entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "loyalty_card_type")
@Data
public class LoyaltyCardType implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "card_name")
    @Size(max = 50)
    @NotNull
    private String cardName;

    @Column(name = "revenue_rank")
    private Double revenueRank;

    @Column(name = "time_card")
    private int timeCard;

    @Column(name = "discount")
    private int discount;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_on")
    private Date updatedOn;
}
