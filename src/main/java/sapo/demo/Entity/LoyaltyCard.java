package sapo.demo.Entity;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "loyalty_card")
@Data
public class LoyaltyCard implements java.io.Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "code")
    @Size(min = 10, max = 20)
    private String code;

    @Column(name = "phone")
    private String phone;

    @Column(name = "loyalty_card_type")
    private Integer loyaltyCardType;

    @Column(name = "point")
    private Double point;

    @Column(name = "revenue")
    private Double revenue;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "created_on")
    private Date createdOn;

    @Column(name = "updated_on")
    private Date updatedOn;
}
