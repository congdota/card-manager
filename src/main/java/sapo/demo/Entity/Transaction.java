package sapo.demo.Entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transaction")
@Data
public class Transaction implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "loyalty_card_id")
    private Integer loyaltyCardId;

    @Column(name = "point_adjust")
    private Double pointAdjust;

    @Column(name = "spent_adjust")
    private Double spentAdjust;

    @Column(name = "created_on")
    private Date createdOn;
}
