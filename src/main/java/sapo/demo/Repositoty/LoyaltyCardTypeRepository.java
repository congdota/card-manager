package sapo.demo.Repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import sapo.demo.Entity.LoyaltyCardType;

public interface LoyaltyCardTypeRepository extends JpaRepository<LoyaltyCardType, Integer> {
}
