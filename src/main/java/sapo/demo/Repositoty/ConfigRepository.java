package sapo.demo.Repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import sapo.demo.Entity.Config;


public interface ConfigRepository extends JpaRepository<Config, Integer> {
    boolean existsByConfigPoint(Double point);
    boolean existsById(Integer id);
}
