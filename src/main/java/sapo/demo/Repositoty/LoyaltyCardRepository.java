package sapo.demo.Repositoty;

import org.springframework.data.jpa.repository.JpaRepository;
import sapo.demo.Entity.LoyaltyCard;

public interface LoyaltyCardRepository extends JpaRepository<LoyaltyCard, Integer> {

}
