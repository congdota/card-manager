package sapo.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sapo.demo.Entity.Config;
import sapo.demo.Repositoty.ConfigRepository;
import sapo.demo.Service.ConfigService;
import lombok.val;

import java.util.Objects;

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Autowired
    private ConfigService configService;
    @Autowired
    private ConfigRepository repo;

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public ResponseEntity<?> add(@RequestBody Config request) {
        val config = repo.existsByConfigPoint(request.getConfigPoint());
        if (config) {
            ResponseEntity.status(HttpStatus.CONFLICT);
        }
        configService.save(request);
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }

    @RequestMapping(value = "/update", method = {RequestMethod.PUT})
    public ResponseEntity<?> update(@RequestBody Config request) {
        val config = repo.existsById(request.getId());
        if (Objects.isNull(config)) {
            ResponseEntity.status(HttpStatus.FORBIDDEN);
        }
        configService.save(request);
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.DELETE})
    public ResponseEntity<?> delete(@RequestParam("id") Integer id) {
        val config = repo.existsById(id);
        if (Objects.isNull(config)) {
            ResponseEntity.status(HttpStatus.FORBIDDEN);
        }
        configService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body("success");
    }
}
