package sapo.demo.Controller;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sapo.demo.Common.CsvUtil;
import sapo.demo.Entity.LoyaltyCard;
import sapo.demo.Repositoty.LoyaltyCardRepository;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

@RestController
@RequestMapping("/loyalty-card")
public class LoyaltyCardController {

    @Autowired
    private LoyaltyCardRepository loyaltyCardRepository;

    @RequestMapping(value = "/upload", method = {RequestMethod.POST})
    public void uploadSimple(@RequestBody InputStream body) throws IOException {
        loyaltyCardRepository.saveAll(CsvUtil.read(LoyaltyCard.class, body));
    }

    @RequestMapping(value = "/upload", method = {RequestMethod.POST}, consumes = "multipart/form-data")
    public void uploadMultipart(@RequestParam("file") MultipartFile importFile) throws IOException {
        try (XSSFWorkbook workbook = new XSSFWorkbook(importFile.getInputStream())) {
            Sheet datatypeSheet = workbook.getSheetAt(1);
            int totalRow = datatypeSheet.getLastRowNum() + 1;
            if (totalRow < 1) return;
            for (int rowIndex = 1; rowIndex < totalRow; rowIndex++) {
                LoyaltyCard loyaltyCard = new LoyaltyCard();
                Row currentRow = datatypeSheet.getRow(rowIndex);
                String code = currentRow.getCell(1) != null ? currentRow.getCell(1).toString() : null;
                String phone = currentRow.getCell(2) != null ? currentRow.getCell(2).toString() : null;
                String loyaltyCardType = currentRow.getCell(3) != null ? currentRow.getCell(3).toString(): null;
                String point = currentRow.getCell(4) != null ? currentRow.getCell(4).toString(): null;
                String revenue = currentRow.getCell(5) != null ? currentRow.getCell(5).toString(): null;
                String startDate = currentRow.getCell(6) != null ? currentRow.getCell(6).toString(): null;
                String endDate = currentRow.getCell(7) != null ? currentRow.getCell(7).toString(): null;
                String createdOn = currentRow.getCell(8) != null ? currentRow.getCell(8).toString(): null;
                String updatedOn = currentRow.getCell(9) != null ? currentRow.getCell(9).toString(): null;
                loyaltyCard.setCode(code);
                loyaltyCard.setPhone(phone);
                loyaltyCard.setLoyaltyCardType(Integer.valueOf(loyaltyCardType));
                loyaltyCard.setPoint(Double.parseDouble(point));
                loyaltyCard.setRevenue(Double.parseDouble(revenue));
            }
        } catch (IOException e) {

        }
    }
}
