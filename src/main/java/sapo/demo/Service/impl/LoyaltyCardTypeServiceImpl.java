package sapo.demo.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import sapo.demo.Entity.LoyaltyCardType;
import sapo.demo.Repositoty.LoyaltyCardTypeRepository;
import sapo.demo.Service.LoyaltyCardTypeService;

public class LoyaltyCardTypeServiceImpl implements LoyaltyCardTypeService {
    @Autowired
    private LoyaltyCardTypeRepository repo;
    @Override
    public void save(LoyaltyCardType loyaltyCardType) {
        repo.save(loyaltyCardType);
    }
}
