package sapo.demo.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sapo.demo.Entity.Config;
import sapo.demo.Repositoty.ConfigRepository;
import sapo.demo.Service.ConfigService;
import lombok.val;

import java.util.Objects;


@Service
@Transactional
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private ConfigRepository repository;

    @Override
    public void save(Config config) {
        repository.save(config);
    }

    @Override
    public void delete(Integer id) {
        val configDto = repository.getOne(id);
        if (Objects.nonNull(configDto)) {
            repository.delete(configDto);
        }
    }
}
