package sapo.demo.Service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import sapo.demo.Entity.LoyaltyCard;
import sapo.demo.Repositoty.LoyaltyCardRepository;
import sapo.demo.Service.LoyaltyCardService;

public class LoyaltyCardServiceImpl implements LoyaltyCardService {

    @Autowired
    private LoyaltyCardRepository repo;

    @Override
    public void save(LoyaltyCard loyaltyCard) {
        repo.save(loyaltyCard);
    }
}
