package sapo.demo.Service;


import sapo.demo.Entity.Config;

public interface ConfigService {
    void save(Config config);

    void delete(Integer id);
}
