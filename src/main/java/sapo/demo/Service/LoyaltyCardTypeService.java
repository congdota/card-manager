package sapo.demo.Service;

import sapo.demo.Entity.LoyaltyCardType;

public interface LoyaltyCardTypeService {
    void save(LoyaltyCardType loyaltyCardType);
}
